#ifndef IMAGE_FORMAT_DETECTOR
#define IMAGE_FORMAT_DETECTOR

#include <stdio.h>
#include <string.h>
#include "bmp_serializer.h"
#include "image_io_status.h"

#define IMAGE_FORMAT_COUNT 4

enum read_status parse_image(char *path, FILE *input, struct image *source);
enum write_status write_image(char *path, FILE* output, struct image const *out_image);

#endif
