/*#include <stdio.h>*/
#include "image_format_detector.h"
#include "image_transformer.h"

#define FILES_OPEN_STATUS_COUNT 5
#define STANDALONE_TRANSFORMER ROTATE_CCV_90

enum files_open_status {
    OPEN_OK = 0,
    OPEN_NOT_ENOUGH_ARGS,
    OPEN_TOO_MANY_ARGS,
    OPEN_INPUT_FILE_NOT_FOUND,
    OPEN_OUTPUT_FILE_NOT_FOUND
};

char *open_error_messages[FILES_OPEN_STATUS_COUNT] = {
    [OPEN_NOT_ENOUGH_ARGS] = "Not enough arguments, provide 2 file paths\n",
    [OPEN_TOO_MANY_ARGS] ="Too many arguments. 2 file paths would be enough\n",
    [OPEN_INPUT_FILE_NOT_FOUND] ="Input file path (first argument) is not valid, file not found\n",
    [OPEN_OUTPUT_FILE_NOT_FOUND] ="Output file path (second argument) is not valid, file not found\n"
};

char *read_error_messages[IMAGE_READ_STATUS_COUNT] = {
    [READ_INVALID_PIXELS] ="Image body (pixels) reading from file failed\n",
    [READ_INVALID_HEADER] ="Image header reading from file failed\n",
    [READ_INVALID_FORMAT] ="Image format is invalid\n",
    [READ_UNABLE_TO_ALLOCATE_PIXELS] ="Could not allocate image. Probably not enough memory\n"
};

char *transform_error_messages[IMAGE_TRANSFORM_STATUS_COUNT] = {
    [TRANSFORM_UNABLE_TO_ALLOCATE_PIXELS] = "Could not allocate result image. Probably not enough memory\n"
};

char *write_error_messages[IMAGE_WRITE_STATUS_COUNT] = {
    [WRITE_INVALID_PIXELS] ="Image body (pixels) writing to file failed\n",
    [WRITE_INVALID_HEADER] ="Image header writing to file failed\n",
    [WRITE_INVALID_FORMAT] ="Image format is invalid\n"
};

enum files_open_status argv_is_valid(int argc, char** argv, FILE **input, FILE **output) {
    if (argc < 3) return OPEN_NOT_ENOUGH_ARGS;
    else if (argc > 3) return OPEN_TOO_MANY_ARGS;

    *input = fopen(argv[1], "rb");
    if (*input == NULL) return OPEN_INPUT_FILE_NOT_FOUND;

    *output = fopen(argv[2], "wb");
    if (*output == NULL) return OPEN_OUTPUT_FILE_NOT_FOUND;

    return OPEN_OK;
}

int main( int argc, char** argv ) {
    /*(void) argc; (void) argv; // supress 'unused parameters' warning*/
    FILE *input, *output;
    enum files_open_status op_status = argv_is_valid(argc, argv, &input, &output);
    if (op_status != OPEN_OK) {
        fprintf(stderr, "%s", open_error_messages[op_status]);
        return 1;
    }

    struct image source = {0};
    struct image transformed = {0};

    enum read_status rd_status = parse_image(argv[1], input, &source);
    if (rd_status != READ_OK) {
        fprintf(stderr, "%s", read_error_messages[op_status]);
        return 1;
    }

    enum transform_status tr_status = transform_image(source, &transformed , STANDALONE_TRANSFORMER);
    if (tr_status != TRANSFORM_OK) {
        fprintf(stderr, "%s", transform_error_messages[tr_status]);
        return 1;
    }
    free_image(&source);

    enum write_status wr_status = write_image(argv[2], output, &transformed);
    if (wr_status != WRITE_OK) {
        fprintf(stderr, "%s", write_error_messages[op_status]);
        return 1;
    }

    free_image(&transformed);
    return 0;
}
