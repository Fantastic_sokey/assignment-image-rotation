#include "image_format_detector.h"

enum image_format {
    NOT_SUPPORTED = 0,
    BMP,
    PNG,
    JPEG
    // ...
};

char *image_format_strvalues[IMAGE_FORMAT_COUNT] = {
    "none",
    "bmp",
    "png",
    "jpeg"
};

typedef enum read_status(image_parser)(FILE* in, struct image* img);
typedef enum write_status(image_writer)(FILE* out, struct image const* img);

static image_parser *image_parsers[IMAGE_FORMAT_COUNT] = {
        [NOT_SUPPORTED] = NULL,
        [BMP] = bmp_to_image,
        [PNG] = NULL,
        [JPEG] = NULL
        // ...
};
static image_writer *image_writers[IMAGE_FORMAT_COUNT] = {
        [NOT_SUPPORTED] = NULL,
        [BMP] = image_to_bmp,
        [PNG] = NULL,
        [JPEG] = NULL
        // ...
};

enum image_format detect_image_format(char *path) {
    char *token = strtok(path, ".");
    if (token == NULL || (token = strtok(NULL, ".")) == NULL) return NOT_SUPPORTED;

    for (size_t format = 1; format < IMAGE_FORMAT_COUNT; format++) {
        if ( strcmp(image_format_strvalues[format], token) == 0 ) return (enum image_format) format;
    }

    return NOT_SUPPORTED;
}

enum read_status parse_image(char *path, FILE *input, struct image *source) {
    enum image_format im_format = detect_image_format(path);
    if (im_format != NOT_SUPPORTED && image_parsers[im_format] != NULL) {
        return image_parsers[im_format](input, source);
    }
    return READ_INVALID_FORMAT;
}

enum write_status write_image(char *path, FILE* output, struct image const* out_image) {
    enum image_format im_format = detect_image_format(path);
    if (im_format != NOT_SUPPORTED && image_writers[im_format] != NULL) {
        return image_writers[im_format](output, out_image);
    }
    return WRITE_INVALID_FORMAT;
}
