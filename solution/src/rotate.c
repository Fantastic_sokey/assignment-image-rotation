#include "rotate.h"

enum transform_status rotate_ccv_90(struct image const source, struct image* result) {

    uint64_t tr_width = source.height;
    uint64_t tr_height = source.width;

    *result = create_image(tr_width, tr_height);
    if (result->data == NULL) return TRANSFORM_UNABLE_TO_ALLOCATE_PIXELS;

    for(uint64_t col = 0; col < tr_width; col++ ) {
        for (uint64_t line = 0; line < tr_height; line++ ) {
            set_pixel(result, tr_width - (col+1),line,source.data[tr_height * col + line]);
        }
    }

    return TRANSFORM_OK;
}
