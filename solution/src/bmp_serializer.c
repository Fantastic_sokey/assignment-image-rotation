#include "bmp_serializer.h"

static size_t bmp_calc_padding(int64_t im_width) {
    return im_width % 4;
}

static size_t bmp_write_header(FILE *out, uint32_t width, uint32_t height) {
    size_t bmp_header_size = sizeof(struct bmp_header);
    size_t bmp_image_size = height * (width + bmp_calc_padding(width)) * sizeof(struct pixel);
    struct bmp_header wr_header = (struct bmp_header) {
            .bfType = BF_TYPE,
            .biHeight = height,
            .biWidth = width,
            .bOffBits = bmp_header_size,
            .biSize = BI_SIZE,
            .biPlanes = BI_PLANES,
            .bfileSize = bmp_header_size + bmp_image_size,
            .biCompression = BI_COMPRESSION,
            .bfReserved = BF_RESERVED,
            .biBitCount = BI_BIT_COUNT,
            .biXPelsPerMeter = BI_X_PELS_PER_METER,
            .biYPelsPerMeter = BI_Y_PELS_PER_METER,
            .biClrUsed = BI_CLR_USED,
            .biClrImportant = BI_CLR_IMPORTANT,
            .biSizeImage = bmp_image_size
    };
    return fwrite(&wr_header, sizeof(struct bmp_header), 1, out);
}

enum read_status bmp_to_image(FILE *in, struct image *img) {
    struct bmp_header im_header = {0};

    if (fread(&im_header, sizeof(struct bmp_header), 1, in) != 1) return READ_INVALID_HEADER;
    *img = create_image(im_header.biWidth, im_header.biHeight);
    if (img->data == NULL) {return READ_UNABLE_TO_ALLOCATE_PIXELS;}

    size_t padding = bmp_calc_padding(img->width);
fseek(in, im_header.bOffBits, SEEK_SET);

    for (size_t line = 0; line < img->height; line++){
        if (fread(img->data + (img -> width)*line, sizeof(struct pixel), img->width, in) != img->width) return READ_INVALID_PIXELS;
fseek(in, padding, SEEK_CUR);
}

    return READ_OK;
}

enum write_status image_to_bmp(FILE *out, struct image const *img) {

    if (bmp_write_header(out, img->width, img->height) != 1) return WRITE_INVALID_HEADER;

    size_t padding = bmp_calc_padding(img->width);
    
    for (size_t line = 0; line < img -> height; line++ ) {
        if (fwrite(&img->data[line * img->width], sizeof(struct pixel), img->width, out) != img->width) return WRITE_INVALID_PIXELS;
        for (size_t i = 0; i < padding; i++) putc( 0, out );
    }

    return WRITE_OK;
}
